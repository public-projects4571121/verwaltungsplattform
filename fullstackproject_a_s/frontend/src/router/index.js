import Vue from "vue";
import VueRouter from "vue-router";

Vue.use(VueRouter)

const routes = [
    {
        path: "/",
        name: "Login",
        component: () => import("../View/LoginSeite")
    },
    {
        path: "/TabelleGruppe",
        name: "TabelleGruppe",
        component: () => import("../View/TabelleGruppe")
    },
    {
        path: "/TeilnehmerInBearbeiten",
        name: "TeilnehmerInBearbeiten",
        component: () => import("../View/TeilnehmerInBearbeiten")
    },
    {
        path: "/TabelleRaeume",
        name: "TabelleRaeume",
        component: () => import("../View/TabelleRaeume")
    },
    {
        path: "/TrainerInBearbeiten",
        name: "TrainerInBearbeiten",
        component:() => import("../View/TrainerInBearbeiten")
    },
    {
        path: "/AlleTabellen",
        name: "AlleTabellen",
        component:() => import("../View/AlleTabellen")
    }
]

const router = new VueRouter({
    routes,
})
export default router
import Vue from 'vue'
import Vuex from 'vuex'
import axios from "axios";

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        groups: [],
        rooms: [],
        trainers: [],
        newTrainers: [],
        teilnehmers: [],

        //Management Data
        teilnehmerToUpdate: {},
        groupList: false,
        teilnehmerList: false,
    },
    mutations: {
        setGroups(state, groups) {
            state.groups = groups
        },
        setRooms(state, rooms) {
            state.rooms = rooms
        },
        setTrainers(state, trainers) {
            state.trainers = trainers.reverse()
        },
        setTeilnehmers(state, teilnehmers) {
            state.teilnehmers = teilnehmers.sort(
                (a, b) => a.vorname.localeCompare(b.vorname)
            )
        },
        submitTrainer(state, trainerSubmit) {
            state.trainers = trainerSubmit
        },
        updateTrainer(state, trainerUpdate) {
            state.trainers = trainerUpdate
        },
        updateTeilnehmer(state, teilnehmerUpdate) {
            state.teilnehmers = teilnehmerUpdate
        },
        addTrainerToGroup(state, trainerToGroup) {
            state.groups = trainerToGroup
        },
    },
    actions: {
        async loadAllGroups(context) {
            const response = await axios.get('http://localhost:8080/getGroup')
            context.commit('setGroups', response.data)
        },
        async loadAllRooms(context) {
            const response = await axios.get("http://localhost:8080/getRaum")
            context.commit("setRooms", response.data)
        },
        async loadAllTrainers(context) {
            const response = await axios.get("http://localhost:8080/allTrainer")
            context.commit("setTrainers", response.data)
        },
        async loadAllTeilnehmer(context) {
            try {
                const response = await axios.get("http://localhost:8080/GetAllTeilnehmerIn")
                context.commit("setTeilnehmers", response.data)
                console.log(response)
            } catch (err) {
                console.log(err)
            }
        },
        async addTeilnehmer(context, teilnehmer) {
            try {
                const response = await axios.post("http://localhost:8080/AddTeilnehmerIn", teilnehmer)
                context.commit("addTeilnehmer", response.data)
                console.log(response)
            } catch (err) {
                console.log(err)
            }
        },
        async deleteTeilnehmerById(context, teilnehmerId) {
            try {
                await axios.delete("http://localhost:8080/DeleteTeilnehmerIn/" + teilnehmerId)
            } catch (err) {
                console.log(err)
            }
        },
        async updateTeilnehmer(context, teilnehmer) {
            try {
                const response = await axios.put("http://localhost:8080/updateTeilnehmerIn/", teilnehmer)
                context.commit(("updateTeilnehmer"))
                console.log(response)
            } catch (err) {
                console.log(err)
            }
        },
        async submitTrainer(context, trainer) {
            try {
                const response = await axios.post("http://localhost:8080/postTrainer", trainer)
                context.commit(("saveTrainer"))
                console.log(response)
            } catch (err) {
                console.log(err)
            }
        },
        async updateTrainer(context, trainers) {
            try {
                const response = await axios.put("http://localhost:8080/updateTrainer/", trainers)
                context.commit(("updateTrainer"))
                console.log(response)
            } catch (err) {
                console.log(err)
            }
        },
        async deleteTrainerById(context, trainerArrCopy) {
            try {
                await axios.delete("http://localhost:8080/trainerdelete/" + trainerArrCopy);
            } catch (err) {
                console.log(err)
            }
        },
        async addTrainerToGroup(context, data) {
            try {
                await axios.put("http://localhost:8080/AddTrainerZuGruppeByID/" + data.groupId + "/" + data.trainerId)
                console.log(context)
            } catch (err) {
                console.log(err)
            }
        },
        async addTeilnehmerToGroup(context, groupTeilnehmerId) {
            try {
                const response = await axios.put("http://localhost:8080/addTeilnehmerZuGruppeById/" + groupTeilnehmerId.groupId + "/" + groupTeilnehmerId.teilnehmerId)
                console.log(response)
            } catch (err) {
                console.log(err)
            }
        }
    }
})



import Vue from 'vue'
import App from './App.vue'
import router from "./router";
import store from "./store"
import 'bootstrap'
import 'bootstrap/dist/css/bootstrap.css'
import axios from "axios";
import VueAxios from "vue-axios";
import responsive from 'vue-responsive'

Vue.use(responsive)
Vue.use(VueAxios, axios)

Vue.config.productionTip = false

new Vue({
  render: h => h(App),
  router,
  store
}).$mount('#app')

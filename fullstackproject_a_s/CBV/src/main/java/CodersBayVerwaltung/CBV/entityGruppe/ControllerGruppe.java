package CodersBayVerwaltung.CBV.entityGruppe;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
public class ControllerGruppe {

    @Autowired
    private final ServiceGruppe serviceGruppe;

    public ControllerGruppe(ServiceGruppe serviceGruppe) {
        this.serviceGruppe = serviceGruppe;
    }

    @PostMapping("postGroup")
    public String postGroup(@RequestBody EntityGruppe entityGruppe) {
        serviceGruppe.postGroup(entityGruppe);
        return "Gruppe wurde hinzugefügt";
    }

    @DeleteMapping("deleteGruppe")
    public String deleteGruppe(@RequestBody EntityGruppe entityGruppe) {
        serviceGruppe.deleteGruppe(entityGruppe);
        return "Gruppe wurde gelöscht.";
    }

    @PutMapping("putGruppe")
    public String putGruppe(@RequestBody EntityGruppe entityGruppe) {
        serviceGruppe.putGruppe(entityGruppe);
        return "Gruppe wurde geupdatet";
    }

    @GetMapping("getGroup")
    public List<EntityGruppe> getGroup() {
        return serviceGruppe.getGroup();
    }

    @PutMapping("AddTrainerZuGruppeByID/{gruppeId}/{trainerId}")
    public String addTrainerToGroup(@PathVariable int gruppeId, @PathVariable int trainerId) {
        serviceGruppe.addTrainerZuGruppe(gruppeId, trainerId);
        return "Ein Trainer wurde zur Gruppe hinzugefügt";
    }

    @PutMapping("addTeilnehmerZuGruppeById/{gruppeId}/{teilnehmerId}")
    public String addTeilnehmerZuGruppe(@PathVariable int gruppeId, @PathVariable int teilnehmerId) {
        serviceGruppe.addTeilnehmerZuGruppe(gruppeId, teilnehmerId);
        return "Ein Teilnehmer wurde hinzugefügt";
    }


}

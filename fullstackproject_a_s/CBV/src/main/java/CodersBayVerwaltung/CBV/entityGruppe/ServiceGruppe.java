package CodersBayVerwaltung.CBV.entityGruppe;

import CodersBayVerwaltung.CBV.entityTeilnehmerIn.EntityTeilnehmerIn;
import CodersBayVerwaltung.CBV.entityTeilnehmerIn.TeilnehmerInCRUDRepository;
import CodersBayVerwaltung.CBV.entityTrainer.EntityTrainer;
import CodersBayVerwaltung.CBV.entityTrainer.TrainerCRUDRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Optional;

@Service
public class ServiceGruppe {

    @Autowired
    GruppeCRUDRepository gruppeCRUDRepository;

    @Autowired
    TrainerCRUDRepository trainerCRUDRepository;

    @Autowired
    TeilnehmerInCRUDRepository teilnehmerInCRUDRepository;


    public void postGroup(EntityGruppe entityGruppe) {
        gruppeCRUDRepository.save(entityGruppe);
    }

    public void deleteGruppe(EntityGruppe entityGruppe) {
        gruppeCRUDRepository.delete(entityGruppe);
    }

    public void putGruppe(EntityGruppe entityGruppe) {
        gruppeCRUDRepository.save(entityGruppe);
    }

    public List<EntityGruppe> getGroup() {
        List<EntityGruppe> getGruppe = (List<EntityGruppe>) gruppeCRUDRepository.findAll();
        return getGruppe;
    }

    public void addTrainerZuGruppe(int gruppeId, int trainerId) {
        EntityTrainer trainer = trainerCRUDRepository.findById(trainerId).get();
        Optional<EntityGruppe> gruppeOpt = gruppeCRUDRepository.findById(gruppeId);
        if (gruppeOpt.isPresent()) {
            if (trainer.getGruppe() != null) {
                EntityGruppe oldGroup = trainer.getGruppe();
                oldGroup.setTrainer(null);
                gruppeCRUDRepository.save(oldGroup);
            }
            trainer.setGruppe(gruppeOpt.get());
            gruppeOpt.get().setTrainer(trainer);
            gruppeCRUDRepository.save(gruppeOpt.get());
            trainerCRUDRepository.save(trainer);
        }
    }

    public void addTeilnehmerZuGruppe(int gruppeId, int teilnhemrId) {
        Optional<EntityGruppe> gruppe = gruppeCRUDRepository.findById(gruppeId);
        EntityTeilnehmerIn teilnehmerIn = teilnehmerInCRUDRepository.findById(teilnhemrId).get();
        gruppe.get().getTeilnehmerIn().add(teilnehmerIn);
        gruppeCRUDRepository.save(gruppe.get());
    }
}
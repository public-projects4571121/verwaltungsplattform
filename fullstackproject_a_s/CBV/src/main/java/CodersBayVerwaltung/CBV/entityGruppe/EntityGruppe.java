package CodersBayVerwaltung.CBV.entityGruppe;

import CodersBayVerwaltung.CBV.entityRaum.EntityRaum;
import CodersBayVerwaltung.CBV.entityTeilnehmerIn.EntityTeilnehmerIn;
import CodersBayVerwaltung.CBV.entityTrainer.EntityTrainer;
import CodersBayVerwaltung.CBV.entityTrainer.Kategorie;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table
public class EntityGruppe {

    public static int nummerFIA;
    public static int nummerSoftware;
    public static int nummerNetzwerk;


    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "raum_id", referencedColumnName = "id")
    @JsonManagedReference
    private EntityRaum raum;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "trainer_id", referencedColumnName = "id")
    @JsonManagedReference
    private EntityTrainer trainer;

    @OneToMany(targetEntity = EntityTeilnehmerIn.class, cascade = CascadeType.ALL)
    @JoinColumn(name = "GruppeId", referencedColumnName = "id")
    private Set<EntityTeilnehmerIn> teilnehmerIn;


    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(nullable = false)
    private int id;

    @Column
    private String name;

    @Column
    private int nummer;

    @Column
    private String start;

    @Column
    private Kategorie kategorie;


    public EntityGruppe(String name, int nummer, Kategorie kategorie, String start) {
        this.name = name;
        this.kategorie = kategorie;
        this.nummer = nummer;
        this.start = start;

        switch (kategorie.name()) {

            case "FIA":
                this.nummer = ++nummerFIA;
                break;

            case "SOFTWARE":
                this.nummer = ++nummerSoftware;
                break;

            case "NETZWERK":
                this.nummer = ++nummerNetzwerk;
                break;

        }
    }


}

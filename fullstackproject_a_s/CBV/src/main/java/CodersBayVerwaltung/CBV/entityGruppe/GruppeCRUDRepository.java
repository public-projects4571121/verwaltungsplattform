package CodersBayVerwaltung.CBV.entityGruppe;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import java.util.List;


@Repository
public interface GruppeCRUDRepository extends CrudRepository<EntityGruppe,Integer> {

    List<EntityGruppe> findAllById(int id);

    List<EntityGruppe> findByKategorie(String kategorie);

    List<EntityGruppe> findByStart(String start);

    List<EntityGruppe> findByNameStartingWith(String prefix);

    List<EntityGruppe> findByNameEndingWith(String suffix);

}
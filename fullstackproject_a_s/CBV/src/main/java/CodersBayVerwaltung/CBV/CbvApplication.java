package CodersBayVerwaltung.CBV;

import CodersBayVerwaltung.CBV.entityGruppe.EntityGruppe;
import CodersBayVerwaltung.CBV.entityGruppe.GruppeCRUDRepository;
import CodersBayVerwaltung.CBV.entityRaum.EntityRaum;
import CodersBayVerwaltung.CBV.entityRaum.RaumCRUDRepository;
import CodersBayVerwaltung.CBV.entityTrainer.Kategorie;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.HashMap;

@SpringBootApplication
public class CbvApplication implements CommandLineRunner {

    @Autowired
    RaumCRUDRepository raumCRUDRepository;
    @Autowired
    GruppeCRUDRepository gruppeCRUDRepository;

    public static void main(String[] args) {
        SpringApplication.run(CbvApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {


        EntityRaum room1 = new EntityRaum(1, "Hedy Lamarr", "Erfinderin");
        EntityRaum room2 = new EntityRaum(2, "Radia Perlman", "Mutter des Internets");
        EntityRaum room3 = new EntityRaum(3, "James Gosling", "Erfinder von JAVA");
        EntityRaum room4 = new EntityRaum(4, "Annie J. Easley", "Informatikerin und Raketenwissenschaftlerin");
        EntityRaum room5 = new EntityRaum(5, "Tim Berners-Lee", "HTML Erfinder");

        HashMap<Integer, EntityRaum> room = new HashMap<Integer, EntityRaum>();

        room.put(room1.getId(), room1);
        room.put(room2.getId(), room2);
        room.put(room3.getId(), room3);
        room.put(room4.getId(), room4);
        room.put(room5.getId(), room5);

        EntityGruppe gruppe1 = new EntityGruppe("FIA", EntityGruppe.nummerFIA, Kategorie.FIA,  "15.08.2022");
        EntityGruppe gruppe2 = new EntityGruppe("NW", EntityGruppe.nummerNetzwerk,Kategorie.NETZWERK,  "22.08.2022");
        EntityGruppe gruppe3 = new EntityGruppe("SW",EntityGruppe.nummerSoftware ,Kategorie.SOFTWARE,  "29.08.2022");
        EntityGruppe gruppe4 = new EntityGruppe("NW",EntityGruppe.nummerNetzwerk ,Kategorie.NETZWERK,  "05.09.2022");
        EntityGruppe gruppe5 = new EntityGruppe("FIA",EntityGruppe.nummerFIA,Kategorie.FIA,  "12.09.2022");

        HashMap<Integer, EntityGruppe> gruppen = new HashMap<Integer, EntityGruppe>();

        gruppen.put(gruppe1.getId(), gruppe1);
        gruppen.put(gruppe2.getId(), gruppe2);
        gruppen.put(gruppe3.getId(), gruppe3);
        gruppen.put(gruppe4.getId(), gruppe4);
        gruppen.put(gruppe5.getId(), gruppe5);



        try {
            raumCRUDRepository.save(room1);
            raumCRUDRepository.save(room2);
            raumCRUDRepository.save(room3);
            raumCRUDRepository.save(room4);
            raumCRUDRepository.save(room5);


        } catch (Exception e) {

            System.err.println("Error beim Speichern der Räume." + e.getMessage());

        }

        try {
            gruppeCRUDRepository.save(gruppe1);
            gruppeCRUDRepository.save(gruppe2);
            gruppeCRUDRepository.save(gruppe3);
            gruppeCRUDRepository.save(gruppe4);
            gruppeCRUDRepository.save(gruppe5);

        } catch (Exception e) {
            System.err.println("Error beim Speichern von Gruppen." + e.getMessage());
        }

        gruppe1.setRaum(room1);
        gruppeCRUDRepository.save(gruppe1);
        gruppe2.setRaum(room2);
        gruppeCRUDRepository.save(gruppe2);
        gruppe3.setRaum(room5);
        gruppeCRUDRepository.save(gruppe3);
        gruppe4.setRaum(room3);
        gruppeCRUDRepository.save(gruppe4);
        gruppe5.setRaum(room4);
        gruppeCRUDRepository.save(gruppe5);

    }
}


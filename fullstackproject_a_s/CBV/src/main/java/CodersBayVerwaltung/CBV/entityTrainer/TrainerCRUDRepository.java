package CodersBayVerwaltung.CBV.entityTrainer;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TrainerCRUDRepository extends CrudRepository<EntityTrainer, Integer> {

    List<EntityTrainer> findAllById(int id);

    List<EntityTrainer> findByKategorie(String kategorie);

    List<EntityTrainer> findByGehalt(double gehlt);

    List<EntityTrainer> findByNameStartingWith(String prefix);

    List<EntityTrainer> findByNameEndingWith(String suffix);
}

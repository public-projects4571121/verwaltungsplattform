package CodersBayVerwaltung.CBV.entityTrainer;

import CodersBayVerwaltung.CBV.entityGruppe.EntityGruppe;
import CodersBayVerwaltung.CBV.entityGruppe.GruppeCRUDRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ServiceTrainer {
    @Autowired
    TrainerCRUDRepository trainerCRUDRepository;

    @Autowired
    GruppeCRUDRepository gruppeCRUDRepository;

    public void postTrainer(EntityTrainer entityTrainer) {
        trainerCRUDRepository.save(entityTrainer);
    }

    public void putTrainer(EntityTrainer entityTrainer) {
        trainerCRUDRepository.save(entityTrainer);
    }

    public Optional<EntityTrainer> getTrainer(int id) {
        return trainerCRUDRepository.findById(id);
    }

    public void deleteTrainer(int id) {
        EntityTrainer trainerToDelete = trainerCRUDRepository.findById(id).get();
        EntityGruppe oldGroup = trainerToDelete.getGruppe();
        if (trainerToDelete.getGruppe() != null) {
            oldGroup.setTrainer(null);
            gruppeCRUDRepository.save(oldGroup);
            trainerCRUDRepository.deleteById(id);
        } else {
            trainerCRUDRepository.deleteById(id);
        }
    }

    public List<EntityTrainer> getAllTrainer() {
        List<EntityTrainer> allTrainer = (List<EntityTrainer>) trainerCRUDRepository.findAll();
        return allTrainer;
    }
}

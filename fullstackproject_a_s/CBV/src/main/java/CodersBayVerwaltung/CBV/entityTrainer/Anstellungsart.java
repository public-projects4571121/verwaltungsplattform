package CodersBayVerwaltung.CBV.entityTrainer;

public enum Anstellungsart {
    VOLLZEIT ("Vollzeit"),
    TEILZEIT ("Teilzeit"),
    GERINGFÜGIG("Geringfügig");

    private final String anstellungsart;



    Anstellungsart(String anstellungsart) {
        this.anstellungsart = anstellungsart;
    }

    public String getAnstellungsart(){
        return getAnstellungsart();
    }
}

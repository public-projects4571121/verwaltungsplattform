package CodersBayVerwaltung.CBV.entityTrainer;

public enum Kategorie {
    SOFTWARE ("Software"),
    NETZWERK ("Netzwerk"),
    FIA ("Fia");

    private final String kategorie;


  Kategorie(String kategorie) {
    this.kategorie = kategorie;
  }

  public String getKategorie(){
    return getKategorie();
  }

}



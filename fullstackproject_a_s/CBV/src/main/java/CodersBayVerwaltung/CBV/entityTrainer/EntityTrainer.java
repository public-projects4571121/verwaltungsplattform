package CodersBayVerwaltung.CBV.entityTrainer;

import CodersBayVerwaltung.CBV.entityGruppe.EntityGruppe;
import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;


@Getter
@Setter
@NoArgsConstructor
@Entity
@Table
public class EntityTrainer {


    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(nullable = false)
    private int id;

    @Column(nullable = false)
    private String name;

    @Column(nullable = false , unique = true)
    private String email;

    @Column(nullable = false)
    private String wohnort;


    @Enumerated(EnumType.STRING)
    private Anstellungsart anstellungsart;

    @Enumerated(EnumType.STRING)
    private Kategorie kategorie;


    @Column(nullable = false)
    private double gehalt;


    @OneToOne(mappedBy = "trainer")
    @JsonBackReference
    private EntityGruppe gruppe;

    public EntityTrainer(String name, String eMailAdresse, String wohnort, Anstellungsart anstellungsart, Kategorie kategorie, double gehalt) {
        this.name = name;
        this.email = eMailAdresse;
        this.wohnort = wohnort;
        this.anstellungsart = anstellungsart;
        this.kategorie = kategorie;
        this.gehalt = gehalt;
    }


}

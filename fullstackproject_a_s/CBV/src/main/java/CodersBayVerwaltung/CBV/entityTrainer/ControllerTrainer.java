package CodersBayVerwaltung.CBV.entityTrainer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin

public class ControllerTrainer {
    @Autowired
    ServiceTrainer serviceTrainer;

    //Constructor
    public ControllerTrainer(ServiceTrainer serviceTrainer) {
        this.serviceTrainer = serviceTrainer;
    }

    @PostMapping("postTrainer")
    public String postTrainer(@RequestBody EntityTrainer entityTrainer){
        serviceTrainer.postTrainer(entityTrainer);
        return "Ein Trainer wurde hinzugefügt";
    }

    @PutMapping("/updateTrainer")
    public String putTrainer(@RequestBody EntityTrainer entityTrainer){
        serviceTrainer.putTrainer(entityTrainer);
        return "Ein Trainer wurde geupdatet";
    }

    @GetMapping("trainer/{id}")
    public Optional<EntityTrainer> getTrainer(@PathVariable int id){
        return serviceTrainer.getTrainer(id);
    }

    @DeleteMapping("/trainerdelete/{id}")
    public String deleteTrainer(@PathVariable int id){
        serviceTrainer.deleteTrainer(id);
        return "Ein Trainer wurde gelöscht";
    }
    @GetMapping("allTrainer")
    public List<EntityTrainer> getAllTrainer(){
        return serviceTrainer.getAllTrainer();
    }
}
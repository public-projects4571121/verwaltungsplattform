package CodersBayVerwaltung.CBV.entityTeilnehmerIn;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table
public class EntityTeilnehmerIn {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @Column(nullable = false)
    private String vorname;

    @Column(nullable = false)
    private String nachname;

    public EntityTeilnehmerIn(String vorname, String nachname){
        this.vorname = vorname;
        this.nachname = nachname;

    }
}
package CodersBayVerwaltung.CBV.entityTeilnehmerIn;

import CodersBayVerwaltung.CBV.entityGruppe.GruppeCRUDRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ServiceTeilnehmerIn {

    @Autowired
    TeilnehmerInCRUDRepository teilnehmerInCRUDRepository;
    @Autowired
    GruppeCRUDRepository gruppeCRUDRepository;

    public void postTeilnehmerIn(EntityTeilnehmerIn entityTeilnehmerIn) {
        teilnehmerInCRUDRepository.save(entityTeilnehmerIn);
    }

    public void putTeilnehmerIn(EntityTeilnehmerIn entityTeilnehmerIn) {
        teilnehmerInCRUDRepository.save(entityTeilnehmerIn);
    }

    // anstatt die variable entityteilnehmer die int id
    public Optional<EntityTeilnehmerIn> getTeilnehmerIn(int id) {
        return teilnehmerInCRUDRepository.findById(id);
    }

    public void deleteTeilnehmerIn(int id) {
        teilnehmerInCRUDRepository.deleteById(id);
    }

    public List<EntityTeilnehmerIn> getAllTeilnehmerIn() {
        List<EntityTeilnehmerIn> allTeilnehmer = (List<EntityTeilnehmerIn>) teilnehmerInCRUDRepository.findAll();
        return allTeilnehmer;
    }
    // not in use!!!
    public String getFirstName(int id) {
        //wenn man etwas mit id zurückgeben will, dann muss OPTIONAL verwendet werden!!
        Optional<EntityTeilnehmerIn> teilnehmer = teilnehmerInCRUDRepository.findById(id);
        return teilnehmer.get().getVorname();
    }
}

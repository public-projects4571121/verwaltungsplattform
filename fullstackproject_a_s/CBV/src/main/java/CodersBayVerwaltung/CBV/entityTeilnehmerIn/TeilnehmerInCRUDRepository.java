package CodersBayVerwaltung.CBV.entityTeilnehmerIn;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TeilnehmerInCRUDRepository extends CrudRepository<EntityTeilnehmerIn, Integer> {



}

package CodersBayVerwaltung.CBV.entityTeilnehmerIn;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin
public class ControllerTeilnehmerIn {

    @Autowired
    ServiceTeilnehmerIn serviceTeilnehmerIn;

    public ControllerTeilnehmerIn(ServiceTeilnehmerIn serviceTeilnehmerIn) {
        this.serviceTeilnehmerIn = serviceTeilnehmerIn;
    }
    @PostMapping("AddTeilnehmerIn")
    public String postTeilnehmerIn(@RequestBody EntityTeilnehmerIn entityTeilnehmerIn){
        serviceTeilnehmerIn.postTeilnehmerIn(entityTeilnehmerIn);
        return "Ein TeilnehmerIn wurde hinzugefügt";
    }

    @PutMapping("updateTeilnehmerIn")
    public String  putTeilnehmerIn(@RequestBody EntityTeilnehmerIn entityTeilnehmerIn){
        serviceTeilnehmerIn.putTeilnehmerIn(entityTeilnehmerIn);
        return "Ein TeilnehmerIn wurde updatet";
    }

    @GetMapping("GetTeilnehmerInById/{id}")
    public Optional <EntityTeilnehmerIn> getTeilnehmerIn(@PathVariable int id){

        return  serviceTeilnehmerIn.getTeilnehmerIn(id);
    }

    @DeleteMapping("DeleteTeilnehmerIn/{id}")
    public String  deleteTeilnehmerIn(@PathVariable int id){
        serviceTeilnehmerIn.deleteTeilnehmerIn(id);
        return "Ein TeilnehmerIn wurde gelöscht";
    }
    @GetMapping("GetAllTeilnehmerIn")
    public List<EntityTeilnehmerIn> getAllTeilnehmerIn(){
        return serviceTeilnehmerIn.getAllTeilnehmerIn();
    }

    @GetMapping("getfirstnameById/{id}")
    public String getFirstName(@PathVariable int id){
        //Vorname Teilnehmer "Ghaith"
        return   serviceTeilnehmerIn.getFirstName(id);
    }
}

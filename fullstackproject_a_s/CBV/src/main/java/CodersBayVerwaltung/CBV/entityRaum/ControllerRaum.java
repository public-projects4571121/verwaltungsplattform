package CodersBayVerwaltung.CBV.entityRaum;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
public class ControllerRaum {

    @Autowired
    private final ServiceRaum serviceRaum;

    public ControllerRaum(ServiceRaum serviceRaum) {
        this.serviceRaum = serviceRaum;
    }

    @GetMapping("getRaum")
    public List<EntityRaum> getRaum(){

        return serviceRaum.getRaum();
    }
}
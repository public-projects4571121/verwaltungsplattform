package CodersBayVerwaltung.CBV.entityRaum;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RaumCRUDRepository extends JpaRepository<EntityRaum, Integer> {
    List<EntityRaum> findByid(int id);

    List<EntityRaum> findByNummer(int nummer);

    List<EntityRaum> findByPersoenlichkeit(String persoenlichkeit);

    List<EntityRaum> deleteEntityRaumByNummer(int nummer);


}

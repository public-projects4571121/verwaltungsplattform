package CodersBayVerwaltung.CBV.entityRaum;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ServiceRaum {

    @Autowired
    RaumCRUDRepository raumCRUDRepository;

    public void postRaum(EntityRaum entityRaum){
        raumCRUDRepository.save(entityRaum);
    }

    public void deleteRaum(EntityRaum entityRaum){
        raumCRUDRepository.delete(entityRaum);
    }

    public void putRaum(EntityRaum entityRaum){
        raumCRUDRepository.save(entityRaum);
    }

    public List<EntityRaum> getRaum(){

        List<EntityRaum> getRaum= raumCRUDRepository.findAll();
        return getRaum;
    }
}

package CodersBayVerwaltung.CBV.entityRaum;

import CodersBayVerwaltung.CBV.entityGruppe.EntityGruppe;
import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


import javax.persistence.*;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table
public class EntityRaum {


    @OneToOne(mappedBy = "raum")
    @JsonBackReference
    private EntityGruppe gruppe;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    @Column(nullable = false, unique = true)
    private int nummer;
    @Column(unique = true)
    private String persoenlichkeit;
    @Column
    private String beschreibung;

    public EntityRaum(int nummer, String persoenlichkeit, String beschreibung) {
        this.nummer = nummer;
        this.beschreibung = beschreibung;
        this.persoenlichkeit = persoenlichkeit;
    }
}
